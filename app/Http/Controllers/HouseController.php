<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Models\House;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class HouseController extends Controller
{
    public function index(SearchRequest $request): Collection
    {

        return House::query()
            ->when($request->filled('name'), function ($q) use ($request) {
                return $q->where('name', 'like', '%' . $request->input('name') . '%');
            })->when($request->filled('price'), function ($q) use ($request) {
                return $q->where('price', '=', $request->input('price'));
            })->when($request->filled('bedrooms'), function ($q) use ($request) {
                return $q->where('bedrooms', '=', $request->input('bedrooms'));
            })->when($request->filled('bathrooms'), function ($q) use ($request) {
                return $q->where('bathrooms', '=', $request->input('bathrooms'));
            })->when($request->filled('storeys'), function ($q) use ($request) {
                return $q->where('storeys', '=', $request->input('storeys'));
            })->when($request->filled('garages'), function ($q) use ($request) {
                return $q->where('garages', '=', $request->input('garages'));
            })->get();
    }
}
